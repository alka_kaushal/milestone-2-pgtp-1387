
import java.io.IOException;

import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;

import Publishers.FM_gpSettlement;
import RESTCalls.RESTCalls;


public class initMain {
		
	public static String mqPropsFilePath = "/home/alka/Documents/M2_1387Rp_Club/Resources/bpl_mq.props";
	public static PropsFileBasedConfiguration mainConfig;

	public static void main(String[] args) throws InterruptedException, IOException {
		
	
		mainConfig = new PropsFileBasedConfiguration(mqPropsFilePath );
		
		/*FM_gpSettlement.initBPLConsumer();
		Thread.sleep(10000);*/
		FM_gpSettlement.initEDSConsumer();
		Thread.sleep(10000);
		
		String inputString = readConsole.readingInput();
		
		switch(inputString){
			case "1":
			//Directly crediting into a player RPS post game play settlement through NFS
			RESTCalls.creditRpPostSettlement(mainConfig.getLongValue("USER_ID"));
			System.out.println("Entered 1");
			break;
			case "2":
			// Check for post gameplay settlement directly from FM to BPL
			FM_gpSettlement.publishGameplaySettlement();
			System.out.println("Entered 2");
			Thread.sleep(1000);	
			break;
			
			case "3":
			// Crediting RPs to a particular player
			RESTCalls.creditRpOnVerification(mainConfig.getLongValue("USER_ID"));
			System.out.println("Entered 3");
			break;
			case "4":
			// Loop to join players with a particular tournament
			for(long no_of_users = mainConfig.getLongValue("NO_OF_USERS"),i=0; no_of_users > 0; no_of_users--,i++){
				
				RESTCalls.joinTournament(mainConfig.getLongValue("USER_ID")+i);
				
			}

			System.out.println("Entered 4");
			
			break;
			case "5":
			// Loop to withdraw players from a tournament
			for(long no_of_users = mainConfig.getLongValue("NO_OF_USERS"),i=0; no_of_users > 0; no_of_users--,i++){
				
				RESTCalls.withdrawTournament(mainConfig.getLongValue("USER_ID")+i);
				
			}
			System.out.println("Entered 5");
			break;
		}
		
	 	
		
		}
	
	

	}


