package RESTCalls;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class RESTCalls {
	
	public static String mqPropsFilePath = "/home/alka/Documents/M2_1387Rp_Club/Resources/bpl_mq.props";
	public static PropsFileBasedConfiguration mainConfig;
	

	public static void joinTournament(long user_id) {
		mainConfig = new PropsFileBasedConfiguration(mqPropsFilePath );
		
		Client client = Client.create();
		WebResource webResource = client
				.resource("http://10.14.24.77:8080/nfs/api/rp/rct");
		
		Gson gson = new Gson();
		JsonObject j = new JsonObject();

		j.addProperty("userId", user_id);
		j.addProperty("tournamentId", mainConfig.getLongValue("TOURNAMENT_ID"));
		j.addProperty("rewardPointValue", mainConfig.getLongValue("RP_VALUE"));
		String js = gson.toJson(j);

		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, js);

		if (response.getStatus() != 201) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

		/*
		 * System.out.println("Output from NFS web service .... \n");
		 * 
		 * String status = response.getEntity(String.class);
		 * System.out.println(status);
		 */

	}
	
	public static void withdrawTournament(long user_id) {
		mainConfig = new PropsFileBasedConfiguration(mqPropsFilePath );

		Client client = Client.create();
		WebResource webResource = client
				.resource("http://10.14.24.77:8080/nfs/api/rp/crpwt");
		
		Gson gson = new Gson();
		JsonObject j = new JsonObject();

		j.addProperty("userId", user_id);
		j.addProperty("tournamentId", mainConfig.getLongValue("TOURNAMENT_ID"));
		j.addProperty("rewardPointValue",mainConfig.getLongValue("RP_VALUE"));
		String js = gson.toJson(j);

		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, js);

		if (response.getStatus() != 201) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}

	}
	
	public static void creditRpOnVerification(long user_id){
		mainConfig = new PropsFileBasedConfiguration(mqPropsFilePath );
		
		Client client = Client.create();
		WebResource webResource = client
				.resource("http://10.14.24.77:8080/nfs/api/rp/crpbpl");
		
		Gson gson = new Gson();
		JsonObject j = new JsonObject();

		j.addProperty("userId", user_id);
		j.addProperty("txnID", mainConfig.getLongValue("TXN_ID"));
		j.addProperty("rewardPointValue",mainConfig.getLongValue("RP_VALUE"));
		String js = gson.toJson(j);
		
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, js);

		if (response.getStatus() != 201) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		
	}
	
	public static void creditRpPostSettlement(long user_id){
		mainConfig = new PropsFileBasedConfiguration(mqPropsFilePath );
		
		Client client = Client.create();
		WebResource webResource = client
				.resource("http://10.14.24.77:8080/nfs/api/rp/crpps");
		
		Gson gson = new Gson();
		JsonObject j = new JsonObject();

		j.addProperty("userId", user_id);
		j.addProperty("tournamentId", mainConfig.getLongValue("TOURNAMENT_ID"));
		j.addProperty("rewardPointValue",mainConfig.getLongValue("RP_VALUE"));
		String js = gson.toJson(j);
		
		ClientResponse response = webResource.type("application/json").post(
				ClientResponse.class, js);

		if (response.getStatus() != 201) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
		}
		
		
		
	}

}
