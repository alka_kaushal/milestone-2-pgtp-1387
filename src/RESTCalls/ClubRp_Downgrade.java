package RESTCalls;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;


public class ClubRp_Downgrade
{
	public static String mqPropsFilePath = "/home/alka/Documents/M2_1387Rp_Club/Resources/db_queries.props";
	private static PropsFileBasedConfiguration mainConfig;
	
	public static void main(String args[]) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		
		mainConfig = new PropsFileBasedConfiguration( mqPropsFilePath );
		DBConnection conn = new DBConnection();
		java.util.Date date = new java.util.Date();
		
		/* 
		 * Setting data for required users
		 * Which includes, updating user_preference,nft_player_accounts,club_status_history
		 * with updated clubs and reward points
		 */
		 
		
		for(long no_of_users = mainConfig.getLongValue("NO_OF_USERS"),i=0; no_of_users > 0; no_of_users--,i++)
		{
		int result1 = conn.getUpdates("update user_preference set club_type="+mainConfig.getIntValue( "SET_CLUB_TYPE" )+" where user_id="+mainConfig.getLongValue( "USER_ID ")+i);
		
		int result2 = conn.getUpdates("update nft_player_accounts set monthly_lp="+mainConfig.getLongValue( "MONTHLY_LP" )+" where user_id="+mainConfig.getLongValue( "USER_ID ")+i);
		
		int result3 = conn.getUpdates("update club_status_history set from_club="+mainConfig.getIntValue("FROM_CLUB")+",to_club="+mainConfig.getIntValue("SET_CLUB_TYPE")+",end_date='"+new Timestamp( date.getTime()) +"' where userid="+(mainConfig.getLongValue("USER_ID")+i)+" order by last_updated desc limit 1");
		
		
		System.out.println(i+"    "+result1+""+result2+""+result3);
	
		}
		
		/*Check if the data is updated by the degradation*/
		System.out.println("Data ready please run the CDC and expect the club to downgrade");
		
		
		for(long no_of_users = mainConfig.getLongValue("NO_OF_USERS"),i=0; no_of_users > 0; no_of_users--,i++){
			
		conn.getResult("select rp_balance from nft_player_accounts where user_id="+mainConfig.getLongValue( "USER_ID ")+i, "rp_balance" );
		
			
		}
	}
	

	
	

}
