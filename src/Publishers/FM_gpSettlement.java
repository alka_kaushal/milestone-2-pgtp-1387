package Publishers;

import Consumers.BPLConsumerHandler;
import Consumers.EDSConsumerHandler;

import java.util.ArrayList;
import java.util.List;

import com.games24x7.common.domainObject.GameContext;
import com.games24x7.common.domainObject.UserContext;
import com.games24x7.format.json.JSONArray;
import com.games24x7.format.json.JSONException;
import com.games24x7.format.json.JSONObject;
import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.mq.MQFrameworkFactory;

public class FM_gpSettlement {
	
	
	public static String mqPropsFilePath = "/home/alka/Documents/M2_1387Rp_Club/Resources/bpl_mq.props";
	public static String EDSmqPropsFilePath = "/home/alka/Documents/M2_1387Rp_Club/Resources/eds_mq.props";
	public static PropsFileBasedConfiguration mainConfig;
	public static PropsFileBasedConfiguration EDSConfig;
	
	public static String bplQueueName = "bpltv";
	
	public static String PLAYER_ID = "playerid";
	public static String AMOUNT = "amount";
	public static String WINNER = "winner";
	public static String SETTLEMENT_TYPE = "settlementtype";
	public static String TOTAL_BUY_IN = "totalbuyin";
	public static String AAID = "aaid";
	public static String TID = "tid";
	public static String ACKNOWLEDGE = "ack";
	public static String TOURN_ID = "tournamentid";
	public static String PRIZE_TYPE = "prizetype";
	public static String MATCH_ID = "matchid";
	public static String REVENUE_PER = "revenuePer";
	public static String REVENUE_EACH = "revenueEach";
	public static String BPL = "bpl";
	public static String PLAYER = "player";
	public static String TYPE = "type";
	
	public static void publishGameplaySettlement(){
		
		List< UserContext > userList = new ArrayList< UserContext >();	
		
		try
		{
		mainConfig = new PropsFileBasedConfiguration(mqPropsFilePath );
		MQFrameworkFactory.init( mainConfig );
		
		MQFrameworkFactory.getFramework().registerQueuePublisher( bplQueueName );
		
		
		GameContext gameContext = new GameContext();
		
		
		
		for(long no_of_users = mainConfig.getLongValue("NO_OF_USERS"),i=0; no_of_users > 0; no_of_users--,i++)
		{
			UserContext userContext = new UserContext();	
			userContext.setUserId(mainConfig.getLongValue("USER_ID")+i);
			userContext.setAmount(100.00);
			userContext.setClientVariant(2);
			userContext.setRevenueContribution(100.00);
			
			userList.add( userContext ); 
		}
		
	
		double revenue = 10.00;
		
		if( mainConfig.getBooleanValue( "LOYALTY_INFO" ) )
		{
			FM_gpSettlement.generateLoyaltyInfo( gameContext, userList, revenue );
			
		}
		
		}	
	
	    catch( Exception e )
		{
		// TODO: handle exception
		e.printStackTrace();
		}
	
	

		
	}
		
	
	
	private static void generateLoyaltyInfo(GameContext gameContext,
			List<UserContext> userList, double revenue) {
	
		System.out.println( "MQPublisher | generateLoyaltyInfo() start" );
		boolean result = false;
		try
		{
			System.out.println("Entered try");
			JSONObject messageObject = new JSONObject();
			JSONArray playerListArray = new JSONArray();
			long tableId = mainConfig.getIntValue("TABLE_ID");
			long tournamentID = mainConfig.getIntValue("TOURN_ID");	
			int settlementType = mainConfig.getIntValue("SETTLEMENT_TYPE");
			
			for( UserContext user : userList )
			{      
				    
				    JSONObject playerlist = new JSONObject();
					playerlist.put(PLAYER_ID, user.getUserId());
					
				playerlist.put( AMOUNT, user.getAmount() );
				
				if( user.getAmount() > 0 )
				{
					playerlist.put( WINNER, true );
				}
				else
				{
					playerlist.put( WINNER, false );
				}
				playerlist.put( "updaterole", user.isUpdateRole() );
				playerlist.put( "channelId", user.getClientVariant() );
				playerlist.put( "revenuecontribution", user.getRevenueContribution() );
				
				messageObject.put( SETTLEMENT_TYPE, mainConfig.getIntValue("SETTLEMENT_TYPE") );
				
				if( settlementType == 2 || settlementType == 3 || settlementType == 4 )
				{
					playerlist.put( TOTAL_BUY_IN, mainConfig.getIntValue("TOTAL_BUY_IN") );
				}

				playerListArray.put( playerlist );
			

			messageObject.put( AAID, mainConfig.getIntValue("AAID_VALUE") );
			messageObject.put( TID, mainConfig.getIntValue("TID_VALUE "));
			messageObject.put( PRIZE_TYPE, mainConfig.getIntValue("PRIZE_TYPE_VALUE"));
			messageObject.put( MATCH_ID, mainConfig.getIntValue("MATCH_ID_VALUE"));
			messageObject.put( REVENUE_PER, mainConfig.getIntValue("REVENUE_PER_VALUE"));
			messageObject.put( TYPE, BPL );
			messageObject.put( PLAYER, playerListArray );
			
			
			if( settlementType == 1 || settlementType == 2 || settlementType == 5 )
			{
				messageObject.put( REVENUE_EACH, ( revenue / userList.size() ) );
			}
			else if( settlementType == 3 || settlementType == 4 )
			{      
				messageObject.put( REVENUE_EACH, ( revenue ) );
			}
			
			
			
		}
			
			result= MQFrameworkFactory.getFramework().publishToQueue( bplQueueName, messageObject.toString());
			
			System.out.println( "Message to BPL: " + messageObject.toString() );
		
			
		}
		
		catch( JSONException e )
		{
			e.printStackTrace();
		}
		
		System.out.println( "MQPublisher | generateLoyaltyInfo() end | result: " + result );
	
		
	}



	public static void initBPLConsumer() {
		
		MQFrameworkFactory.init( new PropsFileBasedConfiguration( mqPropsFilePath ) );
		
		MQFrameworkFactory.getFramework().registerQueueConsumer(bplQueueName, new BPLConsumerHandler());
		
		System.out.println("BPL consumer initialized");
		
	}
	
	
	public static void initEDSConsumer(){
		
		MQFrameworkFactory.init( new PropsFileBasedConfiguration( EDSmqPropsFilePath ) );
		
		
		MQFrameworkFactory.getFramework("EDS_EXCHANGE").registerTopicConsumer("edsTopic", new EDSConsumerHandler());
		
		System.out.println("EDS consumer initialized");
		
		
	}
	
	}

	
